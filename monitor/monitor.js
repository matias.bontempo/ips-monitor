const speedTest = require('speedtest-net');

const monitor = () => new Promise((resolve, reject) => {
  const tStart = new Date();
  const test = speedTest({ maxTime: 5000 });

  test.on('data', (data) => {
    const {
      download, upload, originalDownload, originalUpload,
    } = data.speeds;
    const { ping, id, host } = data.server;
    const excecution = new Date() - tStart;

    const d = {
      download, upload, originalDownload, originalUpload, ping, id, host, excecution,
    };

    resolve(d);
  });

  test.on('error', (err) => {
    reject(err);
  });
});

module.exports = monitor;
