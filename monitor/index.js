const monitor = require('./monitor');
const influxdb = require('./influxdb');

const start = async () => {
  try {
    const data = await monitor();
    await influxdb.write(data);
  } catch (err) {
    console.log('error', err);
  }
};

start();
