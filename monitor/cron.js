require('dotenv').config();

const schedule = require('node-schedule');

const monitor = require('./monitor');
const influxdb = require('./influxdb');

const RUN_EVERY_N_MINUTES = process.env.RUN_EVERY_N_MINUTES || 5;

const start = async () => {
  console.log('Excecuting monitor...');
  try {
    const data = await monitor();
    await influxdb.write(data);
    console.log(`Success: ${data.download} Mbp/s - ${data.upload} Mbp/s`);
  } catch (err) {
    console.log(err);
  }
};

start();
schedule.scheduleJob(`*/${RUN_EVERY_N_MINUTES} * * * *`, start);
