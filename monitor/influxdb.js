require('dotenv').config();

const Influx = require('influx');

const INFLUXDB_HOST = process.env.INFLUXDB_HOST || 'localhost:8086';
const INFLUXDB_DB = process.env.INFLUXDB_DB || 'db0';

const influx = new Influx.InfluxDB({
  host: INFLUXDB_HOST,
  database: INFLUXDB_DB,
  schema: [
    {
      measurement: 'speed_test',
      fields: {
        download: Influx.FieldType.INTEGER,
        upload: Influx.FieldType.INTEGER,
        originalUpload: Influx.FieldType.INTEGER,
        originalDownload: Influx.FieldType.INTEGER,
        ping: Influx.FieldType.FLOAT,
        execution: Influx.FieldType.INTEGER,
      },
      tags: [
        'id',
        'host',
      ],
    },
  ],
});

const write = async ({
  download, upload, originalDownload, originalUpload, ping, execution, id, host,
}) => influx.writePoints([
  {
    measurement: 'speed_test',
    tags: { id, host },
    fields: {
      download, upload, originalDownload, originalUpload, ping, execution,
    },
  },
]);

const read = async () => {
  const rows = await influx.query(`
    SELECT * FROM speed_test
    ORDER BY time DESC
    LIMIT 10
  `);

  console.log(rows);
};

module.exports = { write, read };
